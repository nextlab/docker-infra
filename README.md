
```
git clone git@bitbucket.org:nextlab/docker-infra.git
cd docker-infra
docker-compose up
```

Then you can connect to:
- MongoDB:  127.0.0.1:27017  (user: root & password: Pass123)
- Redis: 127.0.0.1:6379  (password: null)
